#! /bin/sh

# ACU SERVICES STATUS
# PK 2019
# V1.2

# ACU Database : verifie l'URL utilise par l'intra pour recuperer le JSON a afficher.

# Bonne chance pour la piscine !



while true; do

    # FETCHING

    echo -e -n "\e[5mFetching... \e[25m"

    gpush=$(echo $(timeout 10 git push &>/dev/null; echo "%$?")| cut -d'%' -f2-)
    gtags=$(echo $(timeout 10 git push --tags &>/dev/null; echo "%$?")| cut -d'%' -f2-)
    web=$(timeout 10 curl --write-out "%{http_code}\n" --silent --output /dev/null "https://keycloak.assistants.epita.fr")
    api=$(timeout 10 curl --write-out "%{http_code}\n" --silent --output /dev/null "https://intra.assistants.epita.fr/api/exercises/graphql")
    


    # DISPLAY STATUS
    clear

    echo "Last update : $(date +"%T")"
    echo ""


    if [ "$web" != "200" ] && [ "$web" != "302" ] && [ "$web" != "308" ]; then
        echo -e "ACU Website...: \e[48;5;1m DOWN \e[0m ($web)"
    else
        echo -e "ACU Website...: \e[48;5;112m  UP  \e[0m"
    fi


    if [ "$api" != "200" ] && [ "$api" != "302" ] && [ "$api" != "308" ]; then
        echo -e "ACU DataBase..: \e[48;5;1m DOWN \e[0m ($api)"
    else
        echo -e "ACU DataBase..: \e[48;5;112m  UP  \e[0m"
    fi


    if [ "$gpush" != "0" ]; then
        echo -e "GIT Push......: \e[48;5;1m DOWN \e[0m ($gpush)"
    else
        echo -e "GIT Push......: \e[48;5;112m  UP  \e[0m"
    fi


    if [ "$gtags" != "0" ]; then
        echo -e "GIT Tags......: \e[48;5;1m DOWN \e[0m ($gtags)"
    else
        echo -e "GIT Tags......: \e[48;5;112m  UP  \e[0m"
    fi

    
    sleep 10

done